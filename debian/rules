#!/usr/bin/make -f

include /usr/share/dpkg/default.mk

# DH_VERBOSE := 1
export LC_ALL=C.UTF-8

%:
	dh $@ --with python3 --buildsystem=pybuild

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	# apply fix that seems to be needed due to improperly set
	# time zone in pbuilder
	sed -i~ 's/"MTime": "2005-07-07 08:50:00"/"MTime": "2005-07-07 06:50:00"/' tests/common_tests/makefile_test.py

	# Fix binary route to allow testing before installation
	sed -i~ 's/check_run(\["paleomix"\])/check_run([".\/paleomix\/main.py"])/' tests/tools_test/factory_test.py
	find .pybuild -wholename '*paleomix/main.py' -exec chmod -v +x {} \;

	PATH=$(CURDIR)/bin/:$(PATH) PYTHONPATH="$(PYTHONPATH):`pwd`" dh_auto_test

	# Restore
	mv tests/common_tests/makefile_test.py~ tests/common_tests/makefile_test.py
	mv tests/tools_test/factory_test.py~ tests/tools_test/factory_test.py
endif

override_dh_install:
	dh_install
	find debian -name LICENSE -delete

override_dh_fixperms:
	dh_fixperms
	for rs in `find debian -iname "*.r"` ; do \
	    if head -n 1 $$rs | grep -q '^#!/usr/bin/.*Rscript' ; then \
	        chmod +x $$rs ; \
	    else \
	        chmod -x $$rs ; \
	    fi ; \
	done

override_dh_installdocs:
	dh_installdocs
	# since tests dir contains empty files which are used afterwards for
	# testing and dh_installdocs is ignoring empty files we need to make
	# sure all files are really copied
	rsync -a -v tests debian/$(DEB_SOURCE)/usr/share/doc/$(DEB_SOURCE)
	# test runner is seeking for paleomix modules in source - we use inside the installed package
	### sed -i 's:(find paleomix -mindepth 1:(find /usr/lib/python*/dist-packages/paleomix -mindepth 1:' debian/*/usr/share/doc/*/tests/run
	#echo "======  Also empty files belong to the docs ======"
	#find debian -name "empty_file*"
	#find debian -name "timestamp_*"
	#echo "======  Hope something was printed between these lines ===="
	#
	# This test tries to execute setup.sh which does not work outside source directory
	find debian/$(DEB_SOURCE)/usr/share/doc/ -name fileutils_test.py -delete
	find debian/$(DEB_SOURCE)/usr/share/doc/ -name setup.h -delete
